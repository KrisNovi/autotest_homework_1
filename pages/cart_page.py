# здесь описываете методы, которые можно использовать на странице корзины

from hw1.pages.base_page import BasePage
from hw1.utils.locators import *
from selenium.webdriver.common.action_chains import ActionChains

class CartPage(BasePage):
    def __init__(self, driver):
        self.locator = CartPageLocators
        super().__init__(driver)

    def return_items_in_cart(self):
        items_names = []
        items = super().find_elements(*self.locator.items_names)
        for i in items:
            items_names.append(i.text)
        #print('LIST', items_names)
        return items_names

    def delete_all_items(self):
        checkbox_select_all = super().find_element(*self.locator.checkbox_select_all)
        checkbox_select_all.click()
        button_delete = super().find_element(*self.locator.button_delete)
        button_delete.click()



