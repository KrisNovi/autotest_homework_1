from hw1.pages.base_page import BasePage
from hw1.utils.locators import *

class CabinetPage(BasePage):
    def __init__(self, driver): #Это конструктор. Он вызывается когда объект создается
        self.locator = CabinetLocators
        super().__init__(driver)

    def logout(self):
        button_logout = self.find_element(*self.locator.logout_button)
        button_logout.click()