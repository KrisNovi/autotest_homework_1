from hw1.pages.base_page import BasePage
from hw1.utils.locators import *


class DetailedItemPage(BasePage):
    def __init__(self, driver): #Это конструктор. Он вызывается когда объект создается
        self.locator = DetailedItemLocators
        super().__init__(driver)

    def add_to_card(self):
        button_card = self.find_element(*self.locator.button_card)
        button_card.click()

    def get_item_name(self):
        item_name = self.find_element(*self.locator.item_name)
        item_name_text = item_name.text
        #print('SEE', item_name_text)
        return item_name_text

    def is_displayed_hint(self):
        hint = self.find_element(*self.locator.hint)
        return hint.is_displayed()

