# здесь описываете методы, которые можно использовать на странице логина

from hw1.pages.base_page import BasePage
from hw1.utils.locators import *
import time

class LoginPage(BasePage):
    def __init__(self, driver): #Это конструктор. Он вызывается когда объект создается
        self.locator = LoginPageLocators
        super().__init__(driver)
        login_button = super().find_element_with_text(*self.locator.login_button_page)

        login_button.click()
        time.sleep(5)

    def login(self, login, password):
        field_login = super().find_element(*self.locator.field_email)
        field_login.send_keys(login)
        field_password = super().find_element(*self.locator.field_password)
        field_password.send_keys(password)
        time.sleep(3)
        button_login = super().find_element(*self.locator.login_button)
        button_login.click()
        time.sleep(3)

    def clear_fields(self):
        field_login = super().find_element(*self.locator.field_email)
        field_password = super().find_element(*self.locator.field_password)
        field_login.clear()
        field_password.clear()

    def find_not_login_text(self):
        not_login_text = super().find_element(*self.locator.not_login_text)
        return not_login_text.is_displayed()

    def find_login_page(self):
        login_button = super().find_element_with_text(*self.locator.login_button_page)
        return login_button.is_displayed()



