# здесь описываете методы, которые можно использовать везде
from hw1.utils.locators import HeadSiteLocators
import selenium

class BasePage(object):
    def __init__(self, driver, base_url='https://www.sima-land.ru/'):
        self.base_url = base_url
        self.driver = driver
        self.timeout = 30
        self.base_locator = HeadSiteLocators

    def find_element(self, *locator):
        return self.driver.find_element(*locator)

    def find_elements(self, *locator):
        return self.driver.find_elements(*locator)

    def find_element_with_text(self, *locator):
        temp = self.find_elements(*(locator[0:2]))
        result = None
        for el in temp:
            if el.text == locator[2]:
                result = el
        return result

    def open_profile(self):
        profile_button = self.find_element_with_text(*self.base_locator.profile_button)
        profile_button.click()

    def search(self, search_word):
        field_search = self.find_element(*self.base_locator.field_search)
        field_search.send_keys(search_word)
        button_search = self.find_element(*self.base_locator.button_search)
        button_search.click()

    def open_cart(self):
        cart_button = self.find_element(*self.base_locator.cart_button)
        cart_button.click()

    def find_button_profile(self):
        profile_button = self.find_element_with_text(*self.base_locator.profile_button)
        return profile_button.is_displayed()


