from hw1.pages.base_page import BasePage
from hw1.utils.locators import *
from hw1.pages.detailed_item_page import DetailedItemPage
import time

class ItemsPage(BasePage):
    def __init__(self, driver): #Это конструктор. Он вызывается когда объект создается
        self.locator = ItemsLocators
        super().__init__(driver)

    def choose_first_item(self):
        item_name = self.find_element(*self.locator.item_name)
        item_name.click()
        time.sleep(1)
        detailed_item_page = DetailedItemPage(self.driver)
        detailed_item_page.add_to_card()

    def change_view(self):
        icon_grid = self.find_element(*self.locator.icon_grid)
        icon_grid.click()
        time.sleep(3)

    def check_and_set_grid_view(self):
        try:
            icon_grid = self.find_element(*self.locator.icon_grid_check)
        except:
            self.change_view()



