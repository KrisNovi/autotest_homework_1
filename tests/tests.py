# здесь находятся тест-кейсы
# один кейс - один def

import unittest
from hw1.tests.base_test import BaseTest
from hw1.pages.main_page import *
from hw1.utils.test_cases import test_cases
from hw1.pages.login_page import LoginPage
import hw1.resourses.data as data
import time
from hw1.pages.items_page import ItemsPage
from hw1.pages.detailed_item_page import DetailedItemPage
from hw1.pages.cart_page import CartPage
from hw1.pages.cabinet_page import CabinetPage

class TestSima(BaseTest):

    '''def tearDown(self):
        cart_page = CartPage(self.driver)
        cart_page.open_cart()
        items_list = cart_page.return_items_in_cart()
        if len(items_list) > 0:
            cart_page.delete_all_items()
        super().tearDown()'''

    def test_page_load(self):
        print("\n" + str(test_cases(0)))
        page = MainPage(self.driver)
        self.assertTrue(page.check_mainpage_loaded())

    def test_case_01(self):
        print("\n" + str(test_cases(1)))
        page = LoginPage(self.driver)
        page.login(data.data['login'], data.data['wrong_password'])
        time.sleep(3)
        if page.find_not_login_text():
            print('test passed')
        else:
            print('test was not passed')
        page.clear_fields()
        page.login(data.data['login'], data.data['password'])
        time.sleep(3)

        if page.find_button_profile():
            print('test passed')
        else:
            print('test was not passed')

        page.open_profile()
        page_cabinet = CabinetPage(self.driver)
        page_cabinet.logout()

        if page.find_login_page():
            print('test passed')
        else:
            print('test was not passed')

    def test_case_02(self):
        print("\n" + str(test_cases(2)))
        page = LoginPage(self.driver)
        items_page = ItemsPage(self.driver)
        detailed_item_page = DetailedItemPage(self.driver)
        detail_page = DetailedItemPage(self.driver)
        cart_page = CartPage(self.driver)

        page.login(data.data['login'], data.data['password'])
        page.search('Кружка')
        time.sleep(3)
        items_page.check_and_set_grid_view()
        time.sleep(3)
        items_page.choose_first_item()
        time.sleep(1)
        if detailed_item_page.is_displayed_hint():
            print('item was added succesful') #ЭТА ПРОВЕРКА НЕ РАБОТАЕТ
        else:
            print('Item was not added to cart')

        added_item_name = detail_page.get_item_name()
        page.open_cart()
        time.sleep(3)

        names_items_in_cart = cart_page.return_items_in_cart()

        for i in names_items_in_cart:
            if added_item_name in i:
                print('item was added  to cart succesful')
            else:
                print('Item was not added to cart')

    def test_case_03(self):
        print("\n" + str(test_cases(3)))
        page = LoginPage(self.driver)
        items_page = ItemsPage(self.driver)
        detailed_item_page = DetailedItemPage(self.driver)
        cart_page = CartPage(self.driver)
        detail_page = DetailedItemPage(self.driver)
        page_cabinet = CabinetPage(self.driver)
        page.login(data.data['login'], data.data['password'])
        page.search('Кружка')
        time.sleep(3)
        items_page.check_and_set_grid_view()
        time.sleep(3)
        items_page.choose_first_item()
        time.sleep(3)
        added_item_name = detail_page.get_item_name()
        page.open_cart()
        time.sleep(3)
        names_items_in_cart = cart_page.return_items_in_cart()
        for i in names_items_in_cart:
            if added_item_name in i:
                print('item was added  to cart succesful')
            else:
                print('Item was not added to cart')
        page.open_profile()
        time.sleep(2)
        page_cabinet.logout()
        time.sleep(60)
        page = LoginPage(self.driver)
        page.login(data.data['login'], data.data['password'])
        page.open_cart()
        names_items_in_cart = cart_page.return_items_in_cart()
        for i in names_items_in_cart:
            if added_item_name in i:
                print('item was added  to cart succesful')
            else:
                print('Item was not added to cart')

    def test_case_04(self):
        print("\n" + str(test_cases(4)))
        page = LoginPage(self.driver)
        items_page = ItemsPage(self.driver)
        detailed_item_page = DetailedItemPage(self.driver)
        cart_page = CartPage(self.driver)
        detail_page = DetailedItemPage(self.driver)
        page_cabinet = CabinetPage(self.driver)
        page.login(data.data['login'], data.data['password'])
        page.search('Кружка')
        time.sleep(3)
        items_page.check_and_set_grid_view()
        time.sleep(3)
        items_page.choose_first_item()
        time.sleep(1)
        if detailed_item_page.is_displayed_hint():
            print('item was added succesful')  # ЭТА ПРОВЕРКА НЕ РАБОТАЕТ
        else:
            print('Item was not added to cart')
        added_item_name = detail_page.get_item_name()
        page.open_cart()
        names_items_in_cart = cart_page.return_items_in_cart()
        for i in names_items_in_cart:
            if added_item_name in i:
                print('item was added  to cart succesful')
            else:
                print('Item was not added to cart')
        time.sleep(3)
        cart_page.delete_all_items()
        time.sleep(3)
        items_list_after_delete = cart_page.return_items_in_cart()
        assert (not len(items_list_after_delete) != 0)

    def test_case_05(self):
        print("\n" + str(test_cases(5)))
        page = LoginPage(self.driver)
        items_page = ItemsPage(self.driver)
        detailed_item_page = DetailedItemPage(self.driver)
        cart_page = CartPage(self.driver)
        detail_page = DetailedItemPage(self.driver)
        page_cabinet = CabinetPage(self.driver)

        page.login(data.data['login'], data.data['password'])
        page.search('Кружка')
        time.sleep(3)
        items_page.check_and_set_grid_view()
        time.sleep(3)
        items_page.choose_first_item()
        time.sleep(3)
        if detailed_item_page.is_displayed_hint():
            print('item was added succesful')  # ЭТА ПРОВЕРКА НЕ РАБОТАЕТ
        else:
            print('Item was not added to cart')

        added_item_name = detail_page.get_item_name()
        page.open_cart()
        time.sleep(3)

        names_items_in_cart = cart_page.return_items_in_cart()

        for i in names_items_in_cart:
            if added_item_name in i:
                print('item was added  to cart succesful')
            else:
                print('Item was not added to cart')

        time.sleep(2)
        cart_page.delete_all_items()
        time.sleep(1)
        items_list_after_delete = cart_page.return_items_in_cart()
        if len(items_list_after_delete) == 0:
            print('passed')

        page.open_profile()
        time.sleep(3)
        page_cabinet.logout()

        time.sleep(60)
        page = LoginPage(self.driver)
        page.login(data.data['login'], data.data['password'])
        time.sleep(3)
        names_items_in_cart = cart_page.return_items_in_cart()
        if names_items_in_cart == 0:
            print('passed')























