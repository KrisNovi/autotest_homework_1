# запуск и закрытие браузера
# настройки браузера

import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import sys
from hw1.pages.cart_page import CartPage

chromedriver_path = '/home/osboxes/chromedriver'

class BaseTest(unittest.TestCase):

    def setUp(self):
        options = Options()
        # headless режим
        #options.add_argument("--headless")
        options.add_argument('--no-sandbox')
        options.add_argument('disable-infobars')
        options.add_argument("--disable-extensions")
        options.add_argument('--disable-gpu')


        # если не заработает Chrome, переключитесь на FF
        self.driver = webdriver.Chrome(chromedriver_path+'/chromedriver', options=options)
        self.driver.set_window_size(1920, 1080)
        # self.driver = webdriver.Firefox()
        self.driver.get("https://sima-land.ru/")
        time.sleep(5)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    sys.path.append(chromedriver_path)
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPages)
    unittest.TextTestRunner(verbosity=1).run(suite)
