# здесь находятся алиасы до локаторов
# классы логина и корзины необходимо расскоментировать, когда
# будете писать тесты (нельзя иметь пустой класс)

from selenium.webdriver.common.by import By

class MainPageLocators(object):
    LOGO = (By.ID, 'page-header')

class LoginPageLocators(object):
    login_button_page = (By.CSS_SELECTOR, ".link-with-popup__title", "Войти")
    field_email = (By.CSS_SELECTOR, '#login-entity')
    field_password = (By.CSS_SELECTOR, '#login-password')
    login_button = (By.CSS_SELECTOR, 'input[value="Войти"]')
    not_login_text = (By.CSS_SELECTOR, '#login-password-error')

class HeadSiteLocators(object):
    profile_button = (By.CSS_SELECTOR, ".link-with-popup__title", "Профиль")
    field_search = (By.CSS_SELECTOR, 'input[type="Search"]')
    button_search = (By.CSS_SELECTOR, 'button[data-testid="search-field:search-button"]')
    cart_button = (By.CSS_SELECTOR, 'div[class="cart-with-popup__wrapper links-group__item"]')


class CabinetLocators(object):
    logout_button = (By.CSS_SELECTOR, '#logout > span > span')


class ItemsLocators(object):
    item_name = (By.CSS_SELECTOR, 'div[data-testid="item-name"]')
    icon_grid = (By.CSS_SELECTOR, 'svg[aria-label="Малая сетка"]')
    icon_grid_check = (By.CSS_SELECTOR, 'svg[aria-label="Малая сетка"][data-testid*="active"]')


class DetailedItemLocators(object):
    button_card = (By.CSS_SELECTOR, 'div[class="_2QDuw"]')
    item_name = (By.CSS_SELECTOR, 'h1[class="_2o31e"]')
    hint = (By.CSS_SELECTOR, 'div[class="_3doFU"]')


class CartPageLocators(object):
    items_names = (By.CLASS_NAME, 'catalog__item')
    checkbox_select_all = (By.CSS_SELECTOR, '#c12345')
    button_delete = (By.CSS_SELECTOR, 'button[data-action="remove-from-collection"]')



